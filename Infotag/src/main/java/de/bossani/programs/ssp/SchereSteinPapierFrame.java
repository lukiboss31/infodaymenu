package de.bossani.programs.ssp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class SchereSteinPapierFrame extends JFrame {

	private static final String RESOURCE_PATH = "de/bossani/programs/ssp/";
	
	private JPanel contentPane;
	private JButton schere = new JButton("");
	private JButton stein = new JButton("");
	private JButton papier = new JButton("");

	private JLabel ergMensch = new JLabel("");
	private JLabel ergPC = new JLabel("");

	private String e1 = "       ";
	private String e2 = "       ";
	private String erg = "        ";
	private final JLabel lblLabel = new JLabel("Label");
	private final JLabel endstand = new JLabel("0 : 0");

	private int countM = 0;
	private int countPC = 0;

	/**
	 * Create the frame.
	 */
	public SchereSteinPapierFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(getResource("roll-2.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 929, 635);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 213, 57, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		schere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Random random = new Random();
				int ran = anyRandomIntRange(random, 1, 3);
				e1 = "Schere";
				if (ran == 1) {
					lblLabel.setText("Gleichstend!");
					e2 = "Schere";
				}

				else if (ran == 2) {
					lblLabel.setText("Du hast verloren!");
					countPC++;
					e2 = "Stone";
				} else if (ran == 3) {
					lblLabel.setText("Du hast Gewonnen!");
					countM++;
					e2 = "Papier";
				}
				auswertung();

			}
		});
		schere.setIcon(new ImageIcon(getResource("Schere.png")));
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.fill = GridBagConstraints.BOTH;
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 1;
		gbc_button.gridy = 2;
		contentPane.add(schere, gbc_button);

		stein.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Random random = new Random();
				int ran = anyRandomIntRange(random, 1, 3);
				if (ran == 1) {
					lblLabel.setText("Du hast Gewonnen!");
					countM++;
					e2 = "Schere";
				}

				else if (ran == 2) {
					lblLabel.setText("Gleichstend!");
					e2 = "Stone";
				} else if (ran == 3) {
					lblLabel.setText("Du hast verloren!");
					countPC++;
					e2 = "Papier";
				}
				e1 = "Stone";
				auswertung();
			}
		});
		stein.setIcon(new ImageIcon(getResource("Stone.png")));
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.fill = GridBagConstraints.BOTH;
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 3;
		gbc_button_1.gridy = 2;
		contentPane.add(stein, gbc_button_1);

		papier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Random random = new Random();
				int ran = anyRandomIntRange(random, 1, 3);
				if (ran == 1) {
					lblLabel.setText("Du hast verloren!");
					countPC++;
					e2 = "Schere";
				}

				else if (ran == 2) {
					lblLabel.setText("Du hast Gewonnen!");
					countM++;
					e2 = "Stone";
				} else if (ran == 3) {
					lblLabel.setText("Gleichstand!");
					e2 = "Papier";
				}
				e1 = "Papier";
				auswertung();
			}
		});

		papier.setIcon(new ImageIcon(getResource("Papier.png")));
		GridBagConstraints gbc_button_2 = new GridBagConstraints();
		gbc_button_2.fill = GridBagConstraints.BOTH;
		gbc_button_2.insets = new Insets(0, 0, 5, 0);
		gbc_button_2.gridx = 5;
		gbc_button_2.gridy = 2;
		contentPane.add(papier, gbc_button_2);
		lblLabel.setFont(new Font("Tahoma", Font.BOLD, 30));

		lblLabel.setText(erg);
		GridBagConstraints gbc_lblLabel = new GridBagConstraints();
		gbc_lblLabel.gridwidth = 5;
		gbc_lblLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblLabel.gridx = 1;
		gbc_lblLabel.gridy = 3;
		contentPane.add(lblLabel, gbc_lblLabel);

		JLabel lblMensch = new JLabel("Mensch");
		lblMensch.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblMensch = new GridBagConstraints();
		gbc_lblMensch.insets = new Insets(0, 0, 5, 5);
		gbc_lblMensch.gridx = 1;
		gbc_lblMensch.gridy = 4;
		contentPane.add(lblMensch, gbc_lblMensch);

		JLabel lblPc = new JLabel("PC");
		lblPc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblPc = new GridBagConstraints();
		gbc_lblPc.insets = new Insets(0, 0, 5, 0);
		gbc_lblPc.gridx = 5;
		gbc_lblPc.gridy = 4;
		contentPane.add(lblPc, gbc_lblPc);

		ergMensch.setVisible(false);
		GridBagConstraints gbc_lblLabel1 = new GridBagConstraints();
		gbc_lblLabel1.insets = new Insets(0, 0, 5, 5);
		gbc_lblLabel1.gridx = 1;
		gbc_lblLabel1.gridy = 6;
		contentPane.add(ergMensch, gbc_lblLabel1);

		ergPC.setVisible(false);
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 5;
		gbc_label_1.gridy = 6;
		contentPane.add(ergPC, gbc_label_1);

		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridwidth = 5;
		gbc_label.insets = new Insets(0, 0, 0, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 7;
		endstand.setFont(new Font("Tahoma", Font.BOLD, 99));
		contentPane.add(endstand, gbc_label);

	}

	private URL getResource(String name) {
		return getClass().getClassLoader().getResource(RESOURCE_PATH + name);
	}

	public void auswertung() {

		ergMensch.setIcon(new ImageIcon(getResource(e1 + ".png")));
		ergMensch.setVisible(true);

		ergPC.setIcon(new ImageIcon(getResource(e2 + ".png")));
		ergPC.setVisible(true);

		endstand.setText(countM + " : " + countPC);

	}

	public static int anyRandomIntRange(Random random, int low, int high) {
		int randomInt = random.nextInt(high) + low;

		return randomInt;
	}
}
