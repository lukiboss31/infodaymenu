package de.bossani.objects;

import lombok.Data;

@Data
public class MenuItem {
	private String title;
	private String image;
	private String exec;
}
