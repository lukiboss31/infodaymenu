package de.bossani.objects;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import lombok.Getter;

public class MenuButton extends JButton{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1802713498345990968L;
	@Getter
	private MenuItem menuItem;

	public MenuButton(MenuItem menuItem) {
		super();
		this.menuItem = menuItem;
		setVerticalTextPosition(SwingConstants.BOTTOM);
	    setHorizontalTextPosition(SwingConstants.CENTER);
	}
	
}
