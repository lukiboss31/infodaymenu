package de.bossani.objects;

import java.util.List;

import lombok.Data;

@Data
public class Configuration {
	private List<MenuItem> menuItems;
	private String imageDir;
	private String programDir;
	private String title;
}
