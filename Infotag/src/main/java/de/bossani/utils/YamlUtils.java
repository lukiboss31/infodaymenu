package de.bossani.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import de.bossani.objects.Configuration;

/**
 * The Class YamlUtils obtains utils to read and write yaml files. It is used to
 * read and write the configuration file.
 * 
 * @author Lukas Bossani
 */
public class YamlUtils {

	/** The logger. */
	private static Logger LOGGER = Logger.getLogger(YamlUtils.class);

	/**
	 * Copies the basic configuration file to execution directory of the Program to
	 * initialize the basic configuration.
	 * 
	 * @param path the path where the configuration should be copied to.
	 * 
	 */
	public void copyConfigFile(String path) {
		try (InputStream stream = getClass().getClassLoader().getResourceAsStream("config.yml");
				FileOutputStream fos = new FileOutputStream(new File(path));) {
			writeToOs(stream, fos);
		} catch (Exception e) {
			throw new IllegalStateException("unable to copy config.yml", e);
		}
		LOGGER.info("Init finished. Edit config.yml and Start Program with config.yml Path as arg0");
	}

	/**
	 * Read Yaml configuration file and check if it is valid. The File requires
	 * specific structure as initialized in the basic configuration.
	 *
	 * @param configFile the config.yml file
	 * @return the configuration
	 */
	public Configuration readConfiguration(File configFile) {
		try (InputStream in = new FileInputStream(configFile)) {
			Yaml yaml = new Yaml();
			Configuration config = yaml.loadAs(in, Configuration.class);
//			if (!config.isValid()) {
//				throw new IOException();
//			}
//			LOGGER.debug("Config is valid.");
			return config;
		} catch (IOException e) {
			LOGGER.error("Config is not valid. Run jar with arg 'init' to initialize.");
			LOGGER.info("exiting...");
			throw new IllegalStateException("Config is not valid", e);
		}
	}

	/**
	 * Write InputStream in 4kb blocks to OutputStream.
	 *
	 * @param inputStream  the inputStream
	 * @param outputStream the outputStream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void writeToOs(InputStream inputStream, OutputStream outputStream) throws IOException {
		int readBytes;
		byte[] buffer = new byte[4096];
		while ((readBytes = inputStream.read(buffer)) > 0) {
			outputStream.write(buffer, 0, readBytes);
		}
	}

	public void dumConfiguration(Configuration configuration) {
		Yaml yaml = new Yaml();
		FileWriter writer = null;
		try {
			writer = new FileWriter("config.yml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		yaml.dump(configuration, writer);
	}

}
