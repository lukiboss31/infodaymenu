package de.bossani.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.bossani.objects.Configuration;
import de.bossani.objects.MenuButton;
import de.bossani.objects.MenuItem;

import java.awt.GridBagLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.Font;

public class MainFrame extends JFrame {
	private static final String RESOURCE_PATH = "de/bossani/programs/ssp/";
	private static final long serialVersionUID = 5429112497415204206L;
	private static final int IMAGE_SIZE = 256;
	private JPanel contentPane;
	private Configuration configuration;
	private Controller controller;

	/**
	 * Create the frame.
	 */
	public MainFrame(Configuration configuration, Controller controller) {
		this.configuration = configuration;
		this.controller = controller;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setUndecorated(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		int columns = getNoColumns();
		int rows = columns + 1;

		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[columns];
		gbl_contentPane.rowHeights = new int[rows];
		gbl_contentPane.columnWeights = getWeights(columns + 1, 1.0);
		gbl_contentPane.rowWeights = getWeights(rows, 0.0);
		contentPane.setLayout(gbl_contentPane);

		JLabel lblGrb = new JLabel(configuration.getTitle());
		lblGrb.setFont(new Font("Calibri", Font.BOLD, 45));
		GridBagConstraints gbc_lblGrb = new GridBagConstraints();
		gbc_lblGrb.fill = GridBagConstraints.CENTER;
		gbc_lblGrb.insets = new Insets(0, 0, 5, 5);
		gbc_lblGrb.gridx = 0;
		gbc_lblGrb.gridy = 0;
		gbc_lblGrb.gridwidth = columns;
		contentPane.add(lblGrb, gbc_lblGrb);

		int counter = 0;
		for (MenuItem menuItem : configuration.getMenuItems()) {
			JButton button = new MenuButton(menuItem);
			Icon icon = getImage(menuItem);
			button.setText(menuItem.getTitle());
			button.setIcon(icon);
			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.fill = GridBagConstraints.BOTH;
			gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
			gbc_btnNewButton.gridx = counter / columns;
			gbc_btnNewButton.gridy = counter % columns + 1;
			contentPane.add(button, gbc_btnNewButton);
			counter++;
			button.addActionListener(e -> {
				MenuButton btn = (MenuButton) e.getSource();
				controller.onButtonClicked(btn.getMenuItem());
			});
		}
	}

	private Icon getImage(MenuItem menuItem) {
		try {
			Image image;
			if (menuItem.getExec().startsWith("internal.")) {
				image = ImageIO.read(getClass().getClassLoader().getResource(RESOURCE_PATH + "Stone.png"));
			} else {
				if (menuItem.getImage() == null) {
					return null;
				}
				File file = new File(configuration.getImageDir() + menuItem.getImage());
				if (!file.exists()) {
					return null;
				}
				image = ImageIO.read(file);
			}
			return new ImageIcon(resizeIcon(image, IMAGE_SIZE, IMAGE_SIZE));
		} catch (IOException e) {
		}
		return null;
	}

	private double[] getWeights(int columns, double first) {
		double[] weights = new double[columns];
		weights[0] = first;
		for (int i = 1; i < columns; i++) {
			weights[i] = 1.0;
		}
		return weights;
	}

	private int getNoColumns() {
		return (int) Math.sqrt(configuration.getMenuItems().size());
	}

	private static Image resizeIcon(Image img, int resizedWidth, int resizedHeight) {
		double aspectRatio = (double) img.getWidth(null) / (double) img.getHeight(null);
		Image resizedImage = img.getScaledInstance(resizedWidth, (int) (resizedHeight / aspectRatio),
				java.awt.Image.SCALE_SMOOTH);
		return resizedImage;
	}

}
