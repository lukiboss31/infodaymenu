package de.bossani.main;

import java.io.File;
import java.io.IOException;

import de.bossani.objects.Configuration;
import de.bossani.objects.MenuItem;
import de.bossani.programs.ssp.SchereSteinPapierFrame;
import de.bossani.utils.YamlUtils;

public class Controller {

	private static final String CONFIG_YML = "config.yml";
	private MainFrame mainFrame;
	private YamlUtils yamlUtils;
	private Configuration configuration;

	public static void main(String[] args) {
		new Controller().start();
	}

	public void start() {
		checkConfig();
		init();
		mainFrame.setVisible(true);
	}

	private void checkConfig() {
		File file = new File(CONFIG_YML);
		if (!file.exists()) {
			new YamlUtils().copyConfigFile(CONFIG_YML);
		}
	}

	public void init() {
		yamlUtils = new YamlUtils();
		configuration = yamlUtils.readConfiguration(new File(CONFIG_YML));
		createDirs();
		mainFrame = new MainFrame(configuration, this);
	}

	private void createDirs() {
		File imgDir = new File(configuration.getImageDir());
		File progDir = new File(configuration.getProgramDir());
		if (!imgDir.exists()) {
			imgDir.mkdirs();
		}
		if (!progDir.exists()) {
			progDir.mkdirs();
		}
	}

	public void onButtonClicked(MenuItem menuItem) {
		switch (menuItem.getExec()) {
		case "internal.ssp":
			new SchereSteinPapierFrame().setVisible(true);
			return;
		}
		try {
			String command = menuItem.getExec().endsWith(".jar") ? "java -jar " : "";
			Runtime.getRuntime().exec("cmd /c " + command + configuration.getProgramDir() + menuItem.getExec());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
